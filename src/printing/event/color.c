#include "env.h"

t_env	*color(t_env *env, int keycode)
{
	if (keycode == 15)
		env->color = 0x0FF0000;
	else if (keycode == 5)
		env->color = 0x000FF00;
	else
		env->color = 0x0000FF;
	wireframe_iso(env);
	return (env);
}
