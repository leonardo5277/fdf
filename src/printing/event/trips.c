#include "env.h"

t_env	*trips(t_env *env, int keycode)
{
	if (keycode == 123 || keycode == 124)
		env->margex += (keycode == 123 ? -10 : 10);
	else if (keycode == 125 || keycode == 126)
		env->margey += (keycode == 125 ? 10 : -10);
	draw_event(env);
	return (env);
}
